/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.shapeproject;

/**
 *
 * @author OS
 */
public class Square {
    private double wide;
    
    public Square(double wide){
        this.wide = wide;
    }
    
    public double calSquare(){
        return wide * wide;
    }
    
    public double getWide(){
        return wide;
    }
    
    public void  setWide(double wide){
        if (wide <= 0){
            System.out.println("Error : Radius must more than zero!!!!");
            return;
        }
        this.wide = wide;
    }
    @Override
    public String toString(){
        return "Area of Circle1(wide = " + this.getWide() + ") is " + this.calSquare();
    }
}
