/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.shapeproject;

/**
 *
 * @author OS
 */
public class Rectangle {
    private double wide;
    private double length;
    
    public Rectangle(double wide, double length){
        this.wide = wide;
        this.length = length;
    }
  
    public double calRectangle(){
        return wide * length;
    }
    
    public double getLength(){
        return length;
    }
    
    public double getWide(){
        return wide;
    }
    
    public void setRec(double wide, double length){
        if (wide <= 0 || length <= 0){
            System.out.println("Error : wide and length must more than zero!!!!");
            return;
        }else if(wide > length){
            System.out.println("Error : Because wide > length!!!");
            return;
        }
        this.wide = wide;
        this.length = length;
    } 
    @Override
    public String toString(){
        return "Area of Rectangle1(wide = " + this.getWide() + ", length = " + this.getLength() + ") is " + this.calRectangle();
    }
}
