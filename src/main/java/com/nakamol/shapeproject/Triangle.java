/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.shapeproject;

/**
 *
 * @author OS
 */
public class Triangle {
    private double heigth;
    private double base;
    
    public Triangle(double heigth, double base){
        this.heigth = heigth;
        this.base = base;
    }
    
    public double calTriangle(){
        return 0.5 * base * heigth;
    }
    
    public double getHeigth(){
        return heigth;
    }
    
    public double getBase(){
        return base;
    }
    
    public void setTri(double heigth, double base){
        if(heigth <= 0 || base <= 0){
            System.out.println("Error : heigth and base must more than zero!!!!");
            return;
        }
        this.heigth = heigth;
        this.base = base;
    }
    @Override
    public String toString(){
    return "Area of Triangle1(heigth = " + this.getHeigth() + ", base = " + this.getBase() + ") is " + this.calTriangle();
    }
}
